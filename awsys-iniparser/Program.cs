﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace awsys_iniparser
{
    class Program
    {
        static void Main(string[] args)
        {
            try
            {
                INIParser INIParser = new INIParser("Beispiel.ini");
                Console.WriteLine("Der Wert für Allgemein -> Sprache ist: {0}", INIParser.get("Allgemein", "Sprache"));
                Console.WriteLine("Setze Wert auf Englisch: {0}", INIParser.set("Allgemein", "Sprache", "Ungarisch"));
                Console.WriteLine("Der Wert für Allgemein -> Sprache ist: {0}", INIParser.get("Allgemein", "Sprache"));
            }
            catch(Exception e)
            {
                Console.WriteLine("Beim Laden der INI Datei ist ein Fehler aufgetreten: {0}", e.Message);
            }

            Console.ReadKey();
        }
    }
}
