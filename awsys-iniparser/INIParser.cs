﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace awsys_iniparser
{
    class INIParser
    {
        private string FilePath;
        public INIParser(string FilePath)
        {
            if (File.Exists(FilePath))
                this.FilePath = FilePath;
            else
                throw new Exception("Die angegebene Datei wurde nicht gefunden");
        }

        public string get(string Section, string Key)
        {

            return this.parse(Section, Key, "", false);
        }

        public string set(string Section, string Key, string Value)
        {
            return this.parse(Section, Key, Value, true);
        }

        private string parse(string Section, string Key, string Value, bool setValue)
        {
            string IniLine;
            string section = "";
            string key;
            string value;
            string v = null;
            string response;

            bool section_found = false;

            int line = 0;

            FileStream Input = new FileStream(this.FilePath, FileMode.Open, FileAccess.Read, FileShare.ReadWrite);
            StreamReader sr = new StreamReader(Input);

            FileStream Output = new FileStream(this.FilePath, FileMode.Open, FileAccess.Write, FileShare.ReadWrite);
            StreamWriter sw = new StreamWriter(Output);
           
            while ((IniLine = sr.ReadLine()) != null && IniLine.Length != 0)
            {
                line++;

                IniLine = IniLine.Trim();

                if (IniLine.Contains('['))
                {
                    section = IniLine.Substring(IniLine.IndexOf('[') + 1, IniLine.IndexOf(']') - 1);

                    if (Section.ToLower() == section.ToLower())
                    {
                        section_found = true;
                    }
                }

                if (IniLine.Contains('='))
                {
                    int EQPos = IniLine.IndexOf('=');

                    key = IniLine.Substring(0, EQPos);
                    value = IniLine.Substring(EQPos + 1);

                    if (Section.ToLower() == section.ToLower() && Key.ToLower() == key.ToLower())
                    {
                        if (setValue)
                        {
                            IniLine = Key + "=" + Value;
                            v = Value;
                        }
                        else
                        {
                            v = value;
                        }
                    }
                }

                if (setValue)
                {
                    sw.WriteLine(IniLine);
                    sw.Flush();
                }
            }

            sr.Close();
            sw.Close();

            Input.Close();
            Output.Close();

            if (section_found && v != null)
                response = v;
            else if (!section_found)
                response = "Die gewünschte Sektion existiert nicht";
            else
                response = "Die Sektion [" + Section + "] enthält keinen Schlüssel \"" + Key + '"';


            return response;
        }
    }
}
